package model.logic;

import java.io.FileReader;

import java.awt.Point;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.SimpleDateFormat;

import java.util.Date;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.sun.glass.ui.Pixels.Format;

import api.ITaxiTripsManager;
import model.data_structures.Cola;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Pila;
import model.vo.Taxi;
import model.vo.Service;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 
	private Pila<Service> servPila;
	private Cola<Service> servCola;

	public void loadServices(String serviceFile, String taxiId) 
	{
		servCola= new Cola<Service>();
		servPila = new Pila<Service>();
		JsonParser parser = new JsonParser();

		try 
		{
			String taxiTripsDatos = serviceFile;

			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */

			JsonArray arr= (JsonArray) parser.parse(new FileReader(taxiTripsDatos));
			for (int i = 0; arr != null && i < arr.size(); i++)
			{
				JsonObject obj= (JsonObject) arr.get(i);
			
				String tripId = "NaN";

				if ( obj.get("trip_id") != null )
				{ 
					tripId = obj.get("trip_id").getAsString(); 
				}			
				String id = "NaN";
				if ( obj.get("taxi_id") != null )
				{ 
					id = obj.get("taxi_id").getAsString(); 
				}
				Date tripStart  = null ;
				if ( obj.get("trip_start_timestamp") != null )
				{ 
					String a = (obj.get("trip_start_timestamp").getAsString());
					String [] b = a.split("T");
					String[] fecha=b[0].split("-");
					int a�o = Integer.parseInt(fecha[0]);
					int mes = Integer.parseInt(fecha[1]);
					int dia = Integer.parseInt(fecha[2]);
					String []c = b[1].split(":");
					int hora = Integer.parseInt(c[0]);
					int min = Integer.parseInt(c[1]);
					double segD = Double.parseDouble(c[2]);
					int seg = (int) segD;
					tripStart= new Date(a�o, mes, dia,hora,min,seg);
					
				}
				if(id.equalsIgnoreCase(taxiId))
				{
				
					Service a = new Service(taxiId, id, tripStart);
					servPila.push(a);
					servCola.enqueue(a);
				}
			}
		}
			catch (Exception e)
			{

			}
			System.out.println("Inside loadServices with File:" + serviceFile);
			System.out.println("Inside loadServices with TaxiId:" + taxiId);

		}

		@Override
		public int [] servicesInInverseOrder() 
		{
			System.out.println("Inside servicesInInverseOrder");

			int [] resultado = new int[2];
			Service vari = servPila.pop();
			while (true)
			{
				
				Service vari2 = servPila.pop();
				if (vari ==null || vari2 == null)
				{

					break;
				}

				if(vari.getTripStartTime().compareTo(vari2.getTripStartTime()) > 0)
				{
					
					resultado[0]++;
					vari = vari2;
				}
				else 
				{
				
					resultado[1]++ ;
				}


			}

			return resultado;
		}

		@Override
		public int [] servicesInOrder() {
			// TODO Auto-generated method stub
			System.out.println("Inside servicesInOrder");
			int [] resultado = new int[2];
			Service vari = servCola.dequeue();
			while (true)
			{
				Service vari2 = servCola.dequeue();
				if (vari ==null || vari2 == null)
				{
					break;
				}
				if(vari.getTripStartTime().compareTo(vari2.getTripStartTime()) < 0)
				{
					resultado[0]++;
					vari = vari2;
				}
				else 
				{
					resultado[1]++ ;
				}


			}
			return resultado;
		}


	}
