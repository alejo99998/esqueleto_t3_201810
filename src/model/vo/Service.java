package model.vo;

import java.util.Date;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	private String taxi_Id;
	private String trip_Id;
	private Date inicio;
	
	public Service(String pTaxiId, String pTripId,Date pInicio)
	{
		taxi_Id=pTaxiId;
		trip_Id=pTripId;
		inicio= pInicio;
	}
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return trip_Id;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi_Id;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public Date getTripStartTime() 
	{
		return inicio;
	}

	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
