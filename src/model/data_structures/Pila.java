package model.data_structures;

public class Pila<E extends Comparable <E>> implements IStack<E>
{

	private Node<E> tope ;
	private int tama ;

	public Pila()
	{
		tope = null ;
		tama = 0 ;
	}

	@Override
	public void push(E item) 
	{
		if (tope ==null)
		{
			tope = new Node<E>(item) ;
			tama++;

		}
		else
		{
			Node<E> nuevo = new Node<E>(item);
			nuevo.setSig(tope);
			tope = nuevo;
			tama++ ;

		}

	}

	@Override
	public E pop() 
	{
		if ( tope != null )
		{
		tama-- ;
		E vari = tope.getInfo() ;
		tope = tope.getSig() ;
		return vari ;
		}
		else return null;
	}

	@Override
	public boolean isEmpty() 
	{
		return tope ==null?false:true;
	}

}
