package model.data_structures;

public class Node<E>
{

	private Node<E> sig ;
	private E info ;
	
	
	
	public Node(E pInfo)
	{
		info = pInfo;
		sig = null ;
		
	}
	
	
	public E darInfo()
	{
		return info ;
	}


	public Node<E> getSig() 
	{
		return sig;
	}


	public void setSig(Node<E> sig) 
	{
		this.sig = sig;
	}


	public E getInfo() 
	{
		return info;
	}


	public void setInfo(E info) 
	{
		this.info = info;
	}
	
	
	

}
