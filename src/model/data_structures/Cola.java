package model.data_structures;

public class Cola <E extends Comparable <E>> implements IQueue<E>
{

	private Node<E> primero ;
	private Node<E> ultimo ;
	private int tamano ;
	
	public Cola()
	{
		primero = null ;
		ultimo = null ;
		tamano = 0 ;
		
	}
	
	
	
	@Override
	public void enqueue(E item) 
	{
		if (ultimo== null)
		{
			ultimo =new Node<E>(item) ;
			primero = ultimo ;
			tamano++ ;
		}
		else 
		{
			Node<E> vari = new Node<E>(item) ;
			
			ultimo.setSig(vari);
			vari = ultimo ;
			
			tamano++ ;
			
		}
		
	}

	@Override
	public E dequeue() 
	{
		if(primero !=null)
		{
			E vari = primero.getInfo() ;
			
			primero = primero.getSig() ;
			
			return vari ;
		}
		else
		return null;
	}

	@Override
	public boolean isEmpty() 
	{
		return ultimo ==null?false:true;
	}

}
